<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Register Button on Home</name>
   <tag></tag>
   <elementGuidId>98e4aadf-1ad6-4137-bec2-c2a19c15e8b2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>a[@class='nui-navigation-link nui-navigation-register']</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//a[@class='nui-navigation-link nui-navigation-register']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//a[@class='nui-navigation-link nui-navigation-register']</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <type>Main</type>
      <value></value>
   </webElementXpaths>
</WebElementEntity>
