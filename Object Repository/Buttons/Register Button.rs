<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Register Button</name>
   <tag></tag>
   <elementGuidId>bdb624e9-8277-4ed6-b8b0-620e34a33789</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//button[contains(@class,'btn btn-full btn-submit btn-track')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//button[contains(@class,'btn btn-full btn-submit btn-track')]</value>
   </webElementProperties>
</WebElementEntity>
