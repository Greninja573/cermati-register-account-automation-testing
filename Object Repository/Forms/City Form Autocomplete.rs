<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>City Form Autocomplete</name>
   <tag></tag>
   <elementGuidId>d16429db-fd51-493b-a874-6ccca23fca78</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[contains(@class,'autocomplete-list-item highlighted')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[contains(@class,'autocomplete-list-item highlighted')]</value>
   </webElementProperties>
</WebElementEntity>
