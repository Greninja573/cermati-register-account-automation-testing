<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Input Email Form</name>
   <tag></tag>
   <elementGuidId>17fb1970-068a-4021-970c-39501a48f910</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//input[@id='email']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//input[@id='email']</value>
   </webElementProperties>
</WebElementEntity>
