import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

'Open Cermati Website'
WebUI.openBrowser('www.cermati.com')

WebUI.setViewPortSize(1280, 720)

WebUI.delay(3)

'Wait until Register Link Present'
WebUI.waitForElementPresent(findTestObject('Buttons/Register Button on Home'), 3)

'Click Register Link'
WebUI.click(findTestObject('Buttons/Register Button on Home'))

WebUI.waitForPageLoad(5)

WebUI.setText(findTestObject('Forms/Input Email Form'), Email)

WebUI.delay(3)

WebUI.setText(findTestObject('Forms/Input Password Form'), Password)

WebUI.delay(3)

WebUI.setText(findTestObject('Forms/Input Confirm Password Form'), ConfirmationPassword)

WebUI.delay(3)

WebUI.setText(findTestObject('Forms/Input First Name Form'), FirstName)

WebUI.delay(3)

WebUI.setText(findTestObject('Forms/Input Last Name Form'), LastName)

WebUI.delay(3)

WebUI.setText(findTestObject('Forms/Input Phone Number Form'), PhoneNumber)

WebUI.delay(3)

WebUI.setText(findTestObject('Forms/Input City Form'), City)

WebUI.delay(2)

WebUI.focus(findTestObject('Forms/Select Kota Bekasi'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(2)

WebUI.click(findTestObject('Forms/City Form Autocomplete'))

WebUI.delay(5)

WebUI.click(findTestObject('Buttons/Register Button'))

WebUI.waitForPageLoad(3)

WebUI.delay(10)

